using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;

public class Enemy : MonoBehaviour

{
    

    public GameObject target;
    private NavMeshAgent agent;

    //enemy��hp
    private int enemyHp = 3;



    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        target = GameObject.Find("Sphere");
        

    }

    // Update is called once per frame
    void Update()
    {

        if (target)
        {
            agent.destination = target.transform.position;
        }

        

    }

    public void EnemyDamage()
    {
        enemyHp -= 1;

        if (enemyHp == 0)
        {
            Destroy(this.gameObject);
        }
    }


    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Zoneda>())
        {
            Zoneda zoneda = collision.gameObject.GetComponent<Zoneda>();
            zoneda.ZoneDamage();
            Debug.Log(collision.gameObject.name);
            Destroy(gameObject);

        }
    }
}

    