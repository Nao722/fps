using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{

    public Transform shotPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CanShotOn()
    {
        CanShot.canShot = true;
    }

    public void Shooting()
    {
        RaycastHit hitInfo;
        if(Physics.Raycast(shotPos.transform.position,shotPos.transform.forward,out hitInfo,300))
        {
            if(hitInfo.collider.gameObject.GetComponent<Enemy>() != null)
            {
                Enemy hitEnemy = hitInfo.collider.gameObject.GetComponent<Enemy>();
                hitEnemy.EnemyDamage();
            }
        }
    }
}
