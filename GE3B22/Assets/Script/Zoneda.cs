using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Zoneda : MonoBehaviour
{

    public Slider hpBar;
    public int zoneHp = 100;
    // Start is called before the first frame update
    void Start()
    {
        var pos = (Random.onUnitSphere - new Vector3(0.5f, 0.5f, 0.5f)) * 20;
        pos.y = 0;
        transform.position = pos;
        hpBar.value = zoneHp;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ZoneDamage()
    {
        zoneHp -= 20;
        hpBar.value = zoneHp;
        if (zoneHp == 0)
        {
            Destroy(this.gameObject);
            SceneManager.LoadScene("GameOverScene");

        }
    }
}
