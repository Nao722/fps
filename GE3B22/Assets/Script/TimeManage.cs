using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManage : MonoBehaviour
{
    public static int minute;
    public static float seconds;
    // Start is called before the first frame update
    void Start()
    {
        minute = 0;
        seconds = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        seconds += Time.deltaTime;


        if (seconds * 4 >= 60f)
        {
            minute++;
            seconds -= 15;
        }

    }
}
