using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using UnityEngine.UI;


public class PlayerAnim : MonoBehaviour
{
    public Animator animator;
    int ammunition = 9999 ,ammoClip = 20, maxAmmoClip = 20;

    
    
    public Text ammoText;


    // Start is called before the first frame update
    void Start()
    {   
        CanShot.canShot = true;
        
        ammoText.text = ammoClip + "/" + ammunition;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetMouseButton(0) && CanShot.canShot)
        {

            if (ammoClip > 0)
            {
                animator.SetTrigger("Fire");
                CanShot.canShot = false;
                ammoClip--;
                ammoText.text = ammoClip + "/" + ammunition;
            }
            else
            {
                Debug.Log("�e���Ȃ���");
            }


        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            int amountNeed = maxAmmoClip - ammoClip;
            int ammoAvailable = amountNeed < ammunition ? amountNeed : ammunition;
            if(amountNeed != 0 && ammunition  != 0)
            {
                animator.SetTrigger("Reload");
                ammunition -= ammoAvailable;
                ammoClip += ammoAvailable;
                ammoText.text = ammoClip + "/" + ammunition;
            }
           
          
        }
        WalkAnim();
        RunAnim();
    }



   

    public void WalkAnim()
    {
        if (Input.GetKey(KeyCode.W))
        {
            animator.SetBool("Walk", true);
        }
         if (Input.GetKeyUp(KeyCode.W))
         {
            animator.SetBool("Walk", false);
        }
        if (Input.GetKey(KeyCode.A))
        {
            animator.SetBool("Walk", true);
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            animator.SetBool("Walk", false);
        }
        if (Input.GetKey(KeyCode.S))
        {
            animator.SetBool("Walk", true);
         }
        if (Input.GetKeyUp(KeyCode.S))
        {
            animator.SetBool("Walk", false);
         }
        if (Input.GetKey(KeyCode.D))
        {
            animator.SetBool("Walk", true);
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            animator.SetBool("Walk", false);
        }
    }
        
         public void RunAnim()
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                animator.SetBool("Run", true);
            }

            else if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                animator.SetBool("Run", false);
             }
      }
}