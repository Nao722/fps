using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public int min;
    public float seconds;
    public Text scoreText;
    // Start is called before the first frame update
    void Start()
    {
        min = TimeManage.minute;
        seconds = TimeManage.seconds;
        seconds = Mathf.Ceil(seconds);
        seconds *= 4;

        scoreText.text = min + "��" + seconds + "�b�����";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
